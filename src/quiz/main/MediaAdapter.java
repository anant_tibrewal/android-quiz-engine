package quiz.main;

/*
 * Most of this was taken from the Google Android Docs, so it probably isn't the most optimized for this program,
 * but I don't really care as long as it works :p
 * 
 */

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

public class MediaAdapter extends BaseAdapter {
    // references to our images
    private Integer[] mThumbIds = {
            R.drawable.q1_1,
            R.drawable.q1_2,
            R.drawable.q1_3,
            R.drawable.q1_4,
    };
    private Context mContext;
    private int type;
    
    public MediaAdapter(Context c,Integer[] imageList,int typea) {
        mContext = c;
        if (typea == 0){
		    mThumbIds[0] = imageList[0];
		    mThumbIds[1] = imageList[1];
		    mThumbIds[2] = imageList[2];
		    mThumbIds[3] = imageList[3];
        }
        type = typea;
    }

    public int getCount() {
        return mThumbIds.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {
    	if(type == 0){
	        ImageView imageView;
	        if (convertView == null) {  // if it's not recycled, initialize some attributes
	            imageView = new ImageView(mContext);
	            imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
	            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
	            imageView.setPadding(8, 8, 8, 8);
	        } else {
	            imageView = (ImageView) convertView;
	        }
	
	        imageView.setImageResource(mThumbIds[position]);
	        return imageView;
    	}else{
    		ImageView imageView;
	        if (convertView == null) {  // if it's not recycled, initialize some attributes
	            imageView = new ImageView(mContext);
	            imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
	            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
	            imageView.setPadding(8, 8, 8, 8);
	        } else {
	            imageView = (ImageView) convertView;
	        }
	        imageView.setImageResource(R.drawable.replay);
    		return imageView;
    	}
    }
}